<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(['prefix' => 'v1'], function(){
	Route::group(['middleware' => 'api','cors'], function () {
		Route::post('login','APIController@login');
		Route::post('register', 'APIController@register');
		Route::group(['middleware' => 'jwt-auth'], function () {
			
	    	Route::post('get_user_details', 'APIController@get_user_details');
	  	
	    	Route::resource('tipotrabajos', 'TipoTrabajoController');
	    	Route::resource('campos', 'CamposController');
	    	Route::resource('categorias', 'CategoriasController');
	    	Route::resource('clientes', 'ClientesController');
	    	Route::resource('departamentos', 'DepartamentosController');
	    	Route::resource('empleados', 'EmpleadosController');
	    	Route::resource('facturadetalles', 'FacturaDetalleController');
	    	Route::resource('facturas', 'FacturasController');
	    	Route::resource('mantenimientos', 'MantenimientosController');
	    	Route::resource('productos', 'ProductosController');
	    	Route::resource('productomovimientos', 'ProductoMovimientoController');
	    	Route::resource('productomantenimientos', 'ProductoMantenimientoController');
	    	Route::resource('productodetalles', 'ProductoDetalleController');
	    	Route::resource('productosembrados', 'ProductoSiembradoController');
	    	Route::resource('pedidos', 'PedidosController');
	    	Route::resource('pedidodetalles', 'PedidoDetalleController');
	    	Route::resource('pagos', 'PagosController');
	    	Route::resource('tipofracturas', 'TipoFacturaController');
	    	Route::resource('transacciones', 'TransaccionesController');
	    	Route::resource('siembras', 'SiembrasController');

		});
	});
 });
