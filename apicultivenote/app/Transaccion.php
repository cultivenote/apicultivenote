<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    //
     protected $table = 'transacciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'factura_id','pedido_id','consecutivo','fecha_registro',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function pedido(){
        return $this->belongsTo('App\Pedido');
    }
    public function factura(){
        return $this->belongsTo('App\Factura');
    }
}
