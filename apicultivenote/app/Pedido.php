<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    //
     

    protected $table = 'pedidos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cliente_id','descripcion','fecha_registro','estado','fecha_entrega','total',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function transacciones(){
        return $this->hasMany('App\Transaccion');
    }

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    public function pedidodetalles(){
        return $this->hasMany('App\PedidoDetalle');
    }
}
