<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
     protected $table = 'productos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','descripcion','precio','existencia','fecha_vencimiento',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    
    public function productodetalles(){
        return $this->hasMany('App\ProductoDetalle');
    }

    public function productosembrados(){
        return $this->hasMany('App\ProductoSembrado');
    }
    public function pedidodetalles(){
        return $this->hasMany('App\PedidoDetalle');
    }

    public function productomantenimientos(){
        return $this->hasMany('App\ProductoMantenimiento');
    }

     public function productomovimientos(){
        return $this->hasMany('App\ProductoMovimiento');
    }

   public function facturadetalles(){
        return $this->hasMany('App\FacturaDetalle');
    }
}
