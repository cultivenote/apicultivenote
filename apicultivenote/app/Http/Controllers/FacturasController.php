<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Factura;
use Response;
class FacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Factura::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('cliente_id') || !$request->input('codigo_factura') ||!$request->input('consecutivo')||!$request->input('fecha_registro')||!$request->input('total')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevofactura=Factura::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevofactura]), 201)->header('Location', url('/v1/facturas/'.$nuevofactura->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $factura = Factura::find($id);

        if (!$factura) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un factura con ese id.'])],404);
        }

        return response()->json(['data'=>$factura],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $factura=Factura::find($id);
        if (!$factura)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un factura con ese código.'])],404);
        }   

        $cliente=$request->input('cliente_id');
        $codigo=$request->input('codigo_factura');
        $consecutivo=$request->input('consecutivo');
        $fecha_registro=$request->input('fecha_registro');
        $total=$request->input('total');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($cliente)
            {
                $factura->cliente= $cliente;
                $paso=true;
            }
            if ($codigo)
            {
                $factura->codigo_factura= $codigo;
                $paso=true;
            }
            if ($consecutivo)
            {
                $factura->consecutivo= $consecutivo;
                $paso=true;
            }if ($fecha_registro)
            {
                $factura->fecha_registro= $fecha_registro;
                $paso=true;
            }if ($total)
            {
                $factura->total= $total;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $factura->save();
                return response()->json(['data'=>$facturadetalle], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de la factura.'])],304);
            }
        }

        if (!$cliente || !$codigo|| !$consecutivo|| !$fecha_registro|| !$total)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $factura->cliente_id = $cliente;
        $factura->codigo_factura = $codigo;
        $factura->consecutivo = $consecutivo;
        $factura->fecha_registro = $fecha_registro;
        $factura->total = $total;
      

        // Almacenamos en la base de datos el registro.
        $factura->save();
        return response()->json(['data'=>$factura], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
           $factura = Factura::find($id);
        if (!$factura) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un factura con ese código.'])],404);
            }
        else{
         $factura->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
