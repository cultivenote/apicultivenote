<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TipoFactura;
use Response;
class TipoFacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>TipoFactura::all()], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('codigo')|| ! $request->input('descripcion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevotipofactura=TipoFactura::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevotipofactura]), 201)->header('Location', url('/v1/tipofacturas/'.$nuevotipofactura->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $tipofactura = TipoFactura::find($id);

        if (!$tipofactura) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un factura con ese id.'])],404);
        }

        return response()->json(['data'=>$tipofactura],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          $tipofactura=TipoFactura::find($id);
        if (!$tipofactura)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un tipo factura con ese código.'])],404);
        }   
        $codigo=$request->input('codigo');
        $descripcion=$request->input('descripcion');
     
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($descripcion)
            {
                $tipofactura->descripcion= $descripcion;
                $paso=true;
            }
            if ($codigo)
            {
                $tipofactura->codigo= $codigo;
                $paso=true;
            }

          

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $tipofactura->save();
                return response()->json(['data'=>$tipofactura], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de tipo factura.'])],304);
            }
        }

        if (!$descripcion || !$codigo)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $tipofactura->codigo = $codigo;
        $tipofactura->descripcion = $descripcion;
       
        // Almacenamos en la base de datos el registro.
        $tipofactura->save();
        return response()->json(['data'=>$tipofactura], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tipofactura = TipoFactura::find($id);
        if (!$tipofactura) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un tipo factura con ese código.'])],404);
       }
        else{
         $tipofactura->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
