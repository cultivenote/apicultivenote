<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProductoMovimiento;
use App\Producto;
use Response;
class ProductoMovimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productos =Producto::all();
        $productomovimientos = ProductoMovimiento::with('productos');  
        return response()->json(['data'=>$productomovimientos], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('producto_id') || !$request->input('consecutivo') ||!$request->input('cantidad_entrante')||!$request->input('cantidad_saliendo')||!$request->input('descripcion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevodetalle=ProductoMovimiento::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevodetalle]), 201)->header('Location', url('/v1/productomovimientos/'.$nuevodetalle->id))->header('Content-Type', 'application/json');
        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $productomovimiento = ProductoMovimiento::find($id);

        if (!$productomovimiento) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un producto movimiento con ese id.'])],404);
        }

        return response()->json(['data'=>$productomovimiento],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $productomovimiento=ProductoMovimiento::find($id);
        if (!$productomovimiento)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un producto movimiento con ese código.'])],404);
        }   

        $producto=$request->input('producto_id');
        $consecutivo=$request->input('consecutivo');
        $cantidad_entrante=$request->input('cantidad_entrante');
        $cantidad_saliendo=$request->input('cantidad_saliendo');
        $descripcion=$request->input('descripcion');
        

        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($producto)
            {
                $productomovimiento->producto_id= $producto;
                $paso=true;
            }
            if ($consecutivo)
            {
                $productomovimiento->consecutivo= $consecutivo;
                $paso=true;
            }
            if ($cantidad_entrante)
            {
                $productomovimiento->cantidad_entrante= $cantidad_entrante;
                $paso=true;
            }if ($cantidad_saliendo)
            {
                $productomovimiento->cantidad_saliendo= $cantidad_saliendo;
                $paso=true;
            }  
            
            if ($descripcion)
            {
                $productomovimiento->descripcion = $descripcion;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $productomovimiento->save();
                return response()->json(['data'=>$productomovimiento], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de producto movimiento.'])],304);
            }
        }

        if (!$producto || !$consecutivo|| !$cantidad_entrante|| !$cantidad_saliendo|| !$descripcion )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $productomovimiento->producto_id = $producto;
        $productomovimiento->consecutivo = $consecutivo;
        $productomovimiento->cantidad_entrante = $cantidad_entrante;
        $productomovimiento->cantidad_saliendo = $cantidad_saliendo;
        $productomovimiento->descripcion = $descripcion;
      

        // Almacenamos en la base de datos el registro.
        $productomovimiento->save();
        return response()->json(['data'=>$productomovimiento], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productomovimiento = ProductoMovimiento::find($id);
        if (!$productomovimiento) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un producto movimiento con ese código.'])],404);
            }
        else{
         $productomovimiento->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
