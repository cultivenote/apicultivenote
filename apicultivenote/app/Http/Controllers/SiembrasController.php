<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Siembra;
use App\Campo;
use Response;

use App\Http\Requests;

class SiembrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $campos = Campo::all();
        $siembras = Siembra::with('campos');
         return response()->json(['data'=>$siembras], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         if (!$request->input('campo_id') || !$request->input('descripcion') || ! $request->input('consecutivo') || ! $request->input('fecha_registro') ) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevoCampo=Siembra::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevoCampo]), 201)->header('Location', url('/v1/siembras/'.$nuevoCampo->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $siembra = Siembra::find($id);

        if (!$siembra) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un siembra con ese id.'])],404);
        }

        return response()->json(['data'=>$siembra],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $siembra=Siembra::find($id);
        if (!$siembra)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un siembra con ese código.'])],404);
        }   

       
        $campo=$request->input('campo_id');
        $descripcion=$request->input('descripcion');
        $consecutivo=$request->input('consecutivo');
        $fecha_registro=$request->input('fecha_registro');
    
       
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($campo)
            {
                $siembra->campo_id= $campo;
                $paso=true;
            }
            if ($descripcion)
            {
                $siembra->descripcion= $descripcion;
                $paso=true;
            }
            if ($consecutivo)
            {
                $siembra->consecutivo= $consecutivo;
                $paso=true;
            }

            if ($fecha_registro)
            {
                $siembra->fecha_registro = $fecha_registro;
                $paso=true;
            }
            

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $siembra->save();
                return response()->json(['data'=>$siembra], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de siembra.'])],304);
            }
        }

         if (! $request->input('campo_id') || !$request->input('descripcion') || !$request->input('consecutivo') || !$request->input('fecha_registro')) {
        
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
            }

        $siembra->campo_id = $campo;
        $siembra->descripcion = $descripcion;
        $siembra->consecutivo = $consecutivo;
        $siembra->fecha_registro = $fecha_registro;
     
      

        // Almacenamos en la base de datos el registro.
        $siembra->save();
        return response()->json(['data'=>$siembra], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $siembra = Siembra::find($id);
        if (!$siembra) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un siembra con ese código.'])],404);
            }
        else{
         $siembra->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }

    }
}
