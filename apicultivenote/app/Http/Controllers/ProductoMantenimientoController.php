<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProductoMantenimiento;
use App\Mantenimiento;
use App\Producto;
use Response;
class ProductoMantenimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mantenimientos = Mantenimiento::all();
        $productos = Producto::all();
        $productomantenimientos = ProductoMantenimiento::with('mantenimientos'.'productos');
        return response()->json(['data'=>$productomantenimientos], 200);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('producto_id') || !$request->input('mantenimiento_id') ||!$request->input('cantidad')||!$request->input('fecha_aplicado')||!$request->input('observaciones')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevodetalle=ProductoMantenimiento::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevodetalle]), 201)->header('Location', url('/v1/productomantenimientos/'.$nuevodetalle->id))->header('Content-Type', 'application/json');
        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $productom = ProductoMantenimiento::find($id);

        if (!$productom) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un producto mantenimiento con ese id.'])],404);
        }

        return response()->json(['data'=>$productom],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $productom=ProductoMantenimiento::find($id);
        if (!$productom)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un producto mantenimiento con ese código.'])],404);
        }   

        $producto=$request->input('producto_id');
        $mantenimiento=$request->input('mantenimiento_id');
        $cantidad=$request->input('cantidad');
        $fecha_aplicado=$request->input('fecha_aplicado');
        $observaciones=$request->input('observaciones');
       

        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($producto)
            {
                $productom->producto_id= $producto;
                $paso=true;
            }
            if ($mantenimiento)
            {
                $productom->mantenimiento_id= $mantenimiento;
                $paso=true;
            }
            if ($cantidad)
            {
                $productom->cantidad= $cantidad;
                $paso=true;
            }if ($fecha_aplicado)
            {
                $productom->fecha_aplicado= $fecha_aplicado;
                $paso=true;
            }if ($observaciones)
            {
                $productom->observaciones= $observaciones;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $productom->save();
                return response()->json(['data'=>$productom], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de producto mantenimiento.'])],304);
            }
        }

        if (!$producto || !$factura|| !$tipotrabajo|| !$cantidad|| !$precio|| !$gravado|| !$subtotal|| !$descripcion )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $productom->producto_id = $producto;
        $productom->mantenimiento_id = $mantenimiento;
        $productom->cantidad = $cantidad;
        $productom->fecha_aplicado = $fecha_aplicado;
        $productom->observaciones = $observaciones;
      

        // Almacenamos en la base de datos el registro.
        $productom->save();
        return response()->json(['data'=>$productom], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productom = ProductoMantenimiento::find($id);
        if (!$productom) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un producto mantenimiento detalle con ese código.'])],404);
            }
        else{
         $productom->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }

    }
}
