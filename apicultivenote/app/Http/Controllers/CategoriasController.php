<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Categoria;
use Response;
class CategoriasController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         return response()->json(['data'=>Categoria::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('descripcion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevocategoria=Categoria::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevocategoria]), 201)->header('Location', url('/v1/categorias/'.$nuevocategoria->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $categoria = Categoria::find($id);

        if (!$categoria) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un categoria con ese id.'])],404);
        }

        return response()->json(['data'=>$categoria],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $categoria=Categoria::find($id);
        if (!$categoria)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un categoria con ese código.'])],404);
        }   

        $descripcion=$request->input('descripcion');
     
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($descripcion)
            {
                $categoria->descripcion= $descripcion;
                $paso=true;
            }

          

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $categoria->save();
                return response()->json(['data'=>$categoria], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de categoria.'])],304);
            }
        }

        if (!$descripcion )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $categoria->descripcion = $descripcion;
       
      

        // Almacenamos en la base de datos el registro.
        $categoria->save();
        return response()->json(['data'=>$categoria], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $categoria = Categoria::find($id);
        if (!$categoria) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un categoria con ese código.'])],404);
        }
        else{
         $categoria->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
