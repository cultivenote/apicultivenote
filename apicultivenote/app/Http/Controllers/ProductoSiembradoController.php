<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProductoSiembrado;
use App\Producto;
use App\Siembra;
use Response;
class ProductoSiembradoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productos = Producto::all();
        $siembras = Siembra::all();
        $productosembrados= ProductoSiembrado::with('productos','siembras');
        return response()->json(['data'=>$productosembrados], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('sembrado_id') || !$request->input('producto_id') ||!$request->input('cantidad')||!$request->input('fecha_registro')||!$request->input('observaciones')){
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevodetalle=ProductoSiembrado::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevodetalle]), 201)->header('Location', url('/v1/productosembrados/'.$nuevodetalle->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $productosiembrado = ProductoSiembrado::find($id);

        if (!$productosiembrado) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un producto sembrado con ese id.'])],404);
        }

        return response()->json(['data'=>$productosiembrado],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $productosembrado=ProductoSiembrado::find($id);
        if (!$productosembrado)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un factura detalle con ese código.'])],404);
        }   

        $sembrado=$request->input('sembrado_id');
        $producto=$request->input('producto_id');
        $cantidad=$request->input('cantidad');
        $fecha_registro=$request->input('fecha_registro');
        $observaciones=$request->input('observaciones');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($sembrado)
            {
                $productosembrado->sembrado_id= $sembrado;
                $paso=true;
            }
            if ($producto)
            {
                $productosembrado->producto_id= $producto;
                $paso=true;
            }
            if ($cantidad)
            {
                $productosembrado->cantidad= $cantidad;
                $paso=true;
            }
            if ($fecha_registro)
            {
                $productosembrado->fecha_registro= $fecha_registro;
                $paso=true;
            }if ($observaciones)
            {
                $productosembrado->observaciones= $observaciones;
                $paso=true;
            }
            
            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $productosembrado->save();
                return response()->json(['data'=>$productosembrado], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de producto sembrado.'])],304);
            }
        }

        if (!$sembrado || !$producto|| !$cantidad|| !$fecha_registro|| !$observaciones)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $productosembrado->siembra_id = $sembrado;
        $productosembrado->producto_id = $producto;
        $productosembrado->cantidad = $cantidad;
        $productosembrado->fecha_registro = $fecha_registro;
        $productosembrado->observaciones = $observaciones;
     
      

        // Almacenamos en la base de datos el registro.
        $productosembrado->save();
        return response()->json(['data'=>$productosembrado], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $productosembrado = ProductoSiembrado::find($id);
        if (!$productosembrado) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un producto sembrado con ese código.'])],404);
        }
        else{
         $productosembrado->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }

    }
}
