<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pedido;
use Response;
class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Pedido::all()], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('cliente_id') || !$request->input('descripcion')|| !$request->input('fecha_registro')|| !$request->input('estado')|| !$request->input('fecha_entrega')|| !$request->input('total')
            ) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevopedido=Pedido::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevopedido]), 201)->header('Location', url('/v1/pedidos/'.$nuevotipotrabajo->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pedido = Pedido::find($id);

        if (!$pedido) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un pedido con ese id.'])],404);
        }

        return response()->json(['data'=>$pedido],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pedido=Pedido::find($id);
        if (!$pedido)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un pedido con ese código.'])],404);
        }   

        $cliente=$request->input('cliente_id');
        $descripcion=$request->input('descripcion');
        $fecha_registro=$request->input('fecha_registro');
        $estado=$request->input('estado');
        $fecha_entrega=$request->input('fecha_entrega');
        $total=$request->input('total');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($cliente)
            {
                $pedido->cliente_id= $cliente;
                $paso=true;
            }
             if ($descripcion)
            {
                $pedido->descripcion= $descripcion;
                $paso=true;
            }
             if ($fecha_registro)
            {
                $pedido->fecha_registro= $fecha_registro;
                $paso=true;
            }
             if ($estado)
            {
                $pedido->estado= $estado;
                $paso=true;
            }
             if ($fecha_entrega)
            {
                $pedido->fecha_entrega= $fecha_entrega;
                $paso=true;
            }
            if ($total)
            {
                $pedido->total = $total;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $pedido->save();
                return response()->json(['data'=>$pedido], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de pedido.'])],304);
            }
        }

        if (!$cliente || !$descripcion || !$fecha_registro || !$estado || !$fecha_entrega || !$total  )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $pedido->cliente_id= $cliente;
        $pedido->descripcion= $descripcion;
        $pedido->fecha_registro= $fecha_registro;
        $pedido->estado= $estado;
        $pedido->fecha_entrega= $fecha_entrega;
        $pedido->total= $total;

        // Almacenamos en la base de datos el registro.
        $pedido->save();
        return response()->json(['data'=>$pedido], 200);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $pedido = Pedido::find($id);
        if (!$pedido) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un pedido con ese código.'])],404);
            }
        else{
         $pedido->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
