<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TipoTrabajo;
use Response;
class TipoTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>TipoTrabajo::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('descripcion') || !$request->input('precio')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevotipotrabajo=TipoTrabajo::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevotipotrabajo]), 201)->header('Location', url('/v1/tipotrabajos/'.$nuevotipotrabajo->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tipotrabajo = TipoTrabajo::find($id);

        if (!$tipotrabajo) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un tipotrabajo con ese id.'])],404);
        }

        return response()->json(['data'=>$tipotrabajo],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $tipotrabajo=TipoTrabajo::find($id);
        if (!$tipotrabajo)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un tipotrabajo con ese código.'])],404);
        }   

        $descripcion=$request->input('descripcion');
        $precio=$request->input('precio');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($descripcion)
            {
                $tipotrabajo->descripcion= $descripcion;
                $paso=true;
            }

            if ($precio)
            {
                $tipotrabajo->precio = $precio;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $tipotrabajo->save();
                return response()->json(['data'=>$tipotrabajo], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de tipotrabajo.'])],304);
            }
        }

        if (!$descripcion || !$precio )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $tipotrabajo->descripcion = $descripcion;
        $tipotrabajo->precio = $precio;
      

        // Almacenamos en la base de datos el registro.
        $tipotrabajo->save();
        return response()->json(['data'=>$tipotrabajo], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $tipotrabajo = TipoTrabajo::find($id);
        if (!$tipotrabajo) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un tipo trabajo con ese código.'])],404);
       }
        else{
         $tipotrabajo->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }



        
    }
}
