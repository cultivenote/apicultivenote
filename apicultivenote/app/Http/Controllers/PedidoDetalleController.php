<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PedidoDetalle;
use App\Producto;
use App\Pedido;

use Response;
class PedidoDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productos = Producto::all();
        $pedidos = Pedido::all();
        $pedidosdetalles = PedidoDetalle::with('productos','pedidos');
        return response()->json(['data'=>$pedidosdetalles], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('pedido_id') || !$request->input('producto_id')|| !$request->input('cantidad')|| !$request->input('descripcion')|| !$request->input('subtotal')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevopedidodetalle=PedidoDetalle::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevopedidodetalle]), 201)->header('Location', url('/v1/pedidodetalles/'.$nuevopedidodetalle->id))->header('Content-Type', 'application/json');
        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pedidodetalle=PedidoDetalle::find($id);
        if (!$pedidodetalle)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un pedido detalle con ese código.'])],404);
        }   

        $pedido=$request->input('pedido_id');
        $producto=$request->input('producto_id');
        $cantidad=$request->input('cantidad');
        $descripcion=$request->input('descripcion');
        $subtotal=$request->input('subtotal');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($pedido)
            {
                $pedidodetalle->pedido_id= $pedido;
                $paso=true;
            }
             if ($producto)
            {
                $pedidodetalle->producto_id= $producto;
                $paso=true;
            }
             if ($cantidad)
            {
                $pedidodetalle->cantidad= $cantidad;
                $paso=true;
            }
             if ($descripcion)
            {
                $pedidodetalle->descripcion= $descripcion;
                $paso=true;
            }

            if ($subtotal)
            {
                $pedidodetalle->subtotal = $subtotal;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $pedidodetalle->save();
                return response()->json(['data'=>$pedidodetalle], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de pedido detalle.'])],304);
            }
        }

        if (!$pedido || !$producto|| !$cantidad|| !$descripcion|| !$subtotal )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $pedidodetalle->pedido_id = $pedido;
        $pedidodetalle->producto_id = $producto;
        $pedidodetalle->cantidad = $cantidad;
        $pedidodetalle->descripcion = $descripcion;
        $pedidodetalle->subtotal = $subtotal;

        // Almacenamos en la base de datos el registro.
        $pedidodetalle->save();
        return response()->json(['data'=>$pedidodetalle], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $pedidodetalle = PedidoDetalle::find($id);
        if (!$pedidodetalle) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un pedido detalle con ese código.'])],404);
            }
        else{
         $pedidodetalle->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
