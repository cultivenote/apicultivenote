<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProductoDetalle;
use App\Categoria;
use App\Departamento;
use Response;
class ProductoDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $departamentos = Departamento::all();
        $categorias = Categoria::all();
        $productodetalles = ProductoDetalle::with('departamentos','categorias');
        return response()->json(['data'=>$productodetalles ], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('producto_id') || !$request->input('categoria_id')|| !$request->input('departamento_id')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevo=ProductoDetalle::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevo]), 201)->header('Location', url('/v1/productodetalles/'.$nuevo->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $productodetalle = ProductoDetalle::find($id);

        if (!$productodetalle) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un producto detalle con ese id.'])],404);
        }

        return response()->json(['data'=>$productodetalle],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $productodetalle=ProductoDetalle::find($id);
        if (!$productodetalle)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un producto detalle con ese código.'])],404);
        }   

        $producto=$request->input('producto_id');
        $categoria=$request->input('categoria_id');
        $departamento=$request->input('departamento_id');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($producto)
            {
                $productodetalle->producto_id= $producto;
                $paso=true;
            }
             if ($categoria)
            {
                $productodetalle->categoria_id= $categoria;
                $paso=true;
            }
            if ($departamento)
            {
                $productodetalle->departamento_id = $departamento;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $productodetalle->save();
                return response()->json(['data'=>$productodetalle], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de producto detalle.'])],304);
            }
        }

        if (!$producto || !$categoria|| !$departamento )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $productodetalle->producto_id = $producto;
        $productodetalle->categoria_id = $categoria;
        $productodetalle->departamento_id = $departamento;

        // Almacenamos en la base de datos el registro.
        $productodetalle->save();
        return response()->json(['data'=>$productodetalle], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productodetalle = ProductoDetalle::find($id);
        if (!$productodetalle) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un campo con ese código.'])],404);
            }
        else{
         $productodetalle->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
