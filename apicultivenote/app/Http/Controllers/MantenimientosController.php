<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Mantenimiento;
use Response;
class MantenimientosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Mantenimiento::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('campo_id') || !$request->input('descripcion') ||!$request->input('fecha_registro')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevodetalle=Mantenimiento::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevodetalle]), 201)->header('Location', url('/v1/mantenimientos/'.$nuevodetalle->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $mantenimiento = Mantenimiento::find($id);

        if (!$mantenimiento) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un mantenimiento con ese id.'])],404);
        }

        return response()->json(['data'=>$mantenimiento],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $mantenimiento=Mantenimiento::find($id);
        if (!$mantenimiento)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un mantenimiento con ese código.'])],404);
        }   

        $campo=$request->input('campo_id');
        $descripcion=$request->input('descripcion');
        $fecha_registro=$request->input('fecha_registro');
       

        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($campo)
            {
                $mantenimiento->campo_id= $campo;
                $paso=true;
            }
            if ($descripcion)
            {
                $mantenimiento->descripcion= $descripcion;
                $paso=true;
            }
            if ($fecha_registro)
            {
                $mantenimiento->fecha_registro= $fecha_registro;
                $paso=true;
            }
            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $mantenimiento->save();
                return response()->json(['data'=>$mantenimiento], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de mantenimiento.'])],304);
            }
        }

        if (!$campo || !$descripcion|| !$fecha_registro )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $mantenimiento->campo_id = $campo;
        $mantenimiento->descripcion = $descripcion;
        $mantenimiento->fecha_registro = $fecha_registro;


        // Almacenamos en la base de datos el registro.
        $mantenimiento->save();
        return response()->json(['data'=>$mantenimiento], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mantenimiento = Mantenimiento::find($id);
        if (!$mantenimiento) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un mantenimiento con ese código.'])],404);
            }
        else{
         $mantenimiento->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
