<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Empleado;
use Response;
class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Empleado::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('nombre') || !$request->input('cedula')|| !$request->input('telefono')|| !$request->input('direccion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevoempleado=Empleado::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevoempleado]), 201)->header('Location', url('/v1/empleados/'.$nuevoempleado->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $empleado = Empleado::find($id);

        if (!$empleado) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un empleado con ese id.'])],404);
        }

        return response()->json(['data'=>$empleado],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $empleado=Empleado::find($id);
        if (!$empleado)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un empleado con ese código.'])],404);
        }   

        $nombre=$request->input('nombre');
        $cedula=$request->input('cedula');
        $telefono=$request->input('telefono');
        $direccion=$request->input('direccion');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($nombre)
            {
                $empleado->nombre= $nombre;
                $paso=true;
            }

            if ($cedula)
            {
                $empleado->cedula = $cedula;
                $paso=true;
            }
            if ($telefono)
            {
                $empleado->telefono = $telefono;
                $paso=true;
            }
            if ($direccion)
            {
                $empleado->direccion = $direccion;
                $paso=true;
            }
            


            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $empleado->save();
                return response()->json(['data'=>$empleado], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de empleado.'])],304);
            }
        }

        if (!$nombre || !$cedula || !$telefono  || !$direccion  )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $empleado->nombre = $nombre;
        $empleado->cedula = $cedula;
        $empleado->telefono = $telefono;
        $empleado->direccion = $direccion;
      

        // Almacenamos en la base de datos el registro.
        $empleado->save();
        return response()->json(['data'=>$empleado], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $empleado = Empleado::find($id);
        if (!$empleado) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un empleado con ese código.'])],404);
            }
        else{
         $empleado->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
