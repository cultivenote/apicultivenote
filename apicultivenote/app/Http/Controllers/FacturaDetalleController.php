<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\FacturaDetalle;
use App\Factura;
use App\TipoFactura;
use App\Producto;
use Response;

class FacturaDetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $facturas =  Factura::all();
        $tipos = TipoFactura::all();
        $productos =Producto::all();
        $facturadetalles = FacturaDetalle::with('facturas','tipos','productos'); 
        return response()->json(['data'=>$facturadetalles], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         if (! $request->input('producto_id') || !$request->input('factura_id') || !$request->input('tipo_factura_id') || !$request->input('cantidad') || !$request->input('precio_unitario') || !$request->input('gravado_ivi') || !$request->input('subtotal') || !$request->input('descripcion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevodetalle=FacturaDetalle::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevodetalle]), 201)->header('Location', url('/v1/facturadetalles/'.$nuevodetalle->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $facturadetalle = FacturaDetalle::find($id);

        if (!$facturadetalle) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un factura detalle con ese id.'])],404);
        }

        return response()->json(['data'=>$facturadetalle],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $facturadetalle = FacturaDetalle::find($id);
        if (!$facturadetalle)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un factura detalle con ese código.'])],404);
        }   

        $producto=$request->input('producto_id');
        $factura=$request->input('factura_id');
        $tipofactura=$request->input('tipo_factura_id');
        $cantidad=$request->input('cantidad');
        $precio=$request->input('precio_unitario');
        $gravado=$request->input('gravado_ivi');
        $subtotal=$request->input('subtotal');
        $descripcion=$request->input('descripcion');

        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($producto)
            {
                $facturadetalle->producto_id= $producto;
                $paso=true;
            }
            if ($factura)
            {
                $facturadetalle->factura_id= $factura;
                $paso=true;
            }
            if ($tipofactura)
            {
                $facturadetalle->tipo_factura_id= $tipofactura;
                $paso=true;
            }if ($cantidad)
            {
                $facturadetalle->cantidad= $cantidad;
                $paso=true;
            }if ($precio)
            {
                $facturadetalle->precio_unitario= $precio;
                $paso=true;
            }if ($gravado)
            {
                $facturadetalle->gravado_ivi= $gravado;
                $paso=true;
            }if ($subtotal)
            {
                $facturadetalle->subtotal= $subtotal;
                $paso=true;
            }
            if ($descripcion)
            {
                $facturadetalle->descripcion = $descripcion;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $facturadetalle->save();
                return response()->json(['data'=>$facturadetalle], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de factura detalle.'])],304);
            }
        }

        if (!$producto || !$factura|| !$tipotrabajo|| !$cantidad|| !$precio|| !$gravado|| !$subtotal|| !$descripcion )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $facturadetalle->producto_id = $producto;
        $facturadetalle->factura_id = $factura;
        $facturadetalle->tipo_factura_id = $tipofactura;
        $facturadetalle->cantidad = $cantidad;
        $facturadetalle->precio_unitario = $precio;
        $facturadetalle->gravado_ivi = $gravado;
        $facturadetalle->subtotal = $subtotal;
        $facturadetalle->descripcion = $descripcion;
      

        // Almacenamos en la base de datos el registro.
        $facturadetalle->save();
        return response()->json(['data'=>$facturadetalle], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
           $facturadetalle = FacturaDetalle::find($id);
        if (!$facturadetalle) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un factura detalle con ese código.'])],404);
            }
        else{
         $facturadetalle->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
