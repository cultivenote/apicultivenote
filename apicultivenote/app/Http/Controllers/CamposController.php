<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Campo;
use Response;
class CamposController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Campo::all()], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         if (!$request->input('nombre') || !$request->input('duenno') || ! $request->input('title') || ! $request->input('latitud') || ! $request->input('longitud') || !$request->input('descripcion') || !$request->input('direccion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevoCampo=Campo::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevoCampo]), 201)->header('Location', url('/v1/campos/'.$nuevoCampo->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $campo = Campo::find($id);

        if (!$campo) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un campo con ese id.'])],404);
        }

        return response()->json(['data'=>$campo],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
     $campo=Campo::find($id);
        if (!$campo)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un campo con ese código.'])],404);
        }   

       
        $nombre=$request->input('nombre');
        $duenno=$request->input('duenno');
        $tile=$request->input('title');
        $latitud=$request->input('latitud');
        $longitud=$request->input('longitud');
        $descripcion=$request->input('descripcion');
        $direccion=$request->input('direccion');
       
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($title)
            {
                $campo->title= $title;
                $paso=true;
            }
            if ($latitud)
            {
                $campo->latitud= $latitud;
                $paso=true;
            }
            if ($longitud)
            {
                $campo->longitud= $longitud;
                $paso=true;
            }

            if ($nombre)
            {
                $campo->nombre = $nombre;
                $paso=true;
            }
            if ($duenno)
            {
                $campo->duenno = $duenno;
                $paso=true;
            }
            if ($descripcion)
            {
                $campo->descripcion = $descripcion;
                $paso=true;
            }
             if ($direccion)
            {
                $campo->direccion = $direccion;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $campo->save();
                return response()->json(['data'=>$campo], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de campo.'])],304);
            }
        }

         if (!$request->input('nombre') || !$request->input('duenno') || ! $request->input('title') || ! $request->input('latitud') || ! $request->input('longitud') || !$request->input('descripcion') || !$request->input('direccion'))  {
        
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
            }

        $campo->title = $title;
        $campo->latitud = $latitud;
        $campo->longitud = $longitud;
        $campo->nombre = $nombre;
        $campo->duenno = $duenno;
        $campo->descripcion = $descripcion;
        $campo->direccion = $direccion;
      

        // Almacenamos en la base de datos el registro.
        $campo->save();
        return response()->json(['data'=>$campo], 200);


    }   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $campo = Campo::find($id);
        if (!$campo) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un campo con ese código.'])],404);
            }
        else{
         $campo->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
