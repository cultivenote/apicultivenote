<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Departamento;
use Response;
class DepartamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Departamento::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('descripcion')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevodepartamento=Departamento::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevodepartamento]), 201)->header('Location', url('/v1/departamentos/'.$nuevodepartamento->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $departamento = Departamento::find($id);

        if (!$departamento) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un departamento con ese id.'])],404);
        }

        return response()->json(['data'=>$departamento],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $departamento=Departamento::find($id);
        if (!$departamento)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un departamento con ese código.'])],404);
        }   

        $descripcion=$request->input('descripcion');
     
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($descripcion)
            {
                $departamento->descripcion= $descripcion;
                $paso=true;
            }

          

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $departamento->save();
                return response()->json(['data'=>$departamento], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de departamento.'])],304);
            }
        }

        if (!$descripcion )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $departamento->descripcion = $descripcion;
       
      

        // Almacenamos en la base de datos el registro.
        $departamento->save();
        return response()->json(['data'=>$departamento], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
           $departamento = Departamento::find($id);
        if (!$departamento) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un departamento con ese código.'])],404);
            }
        else{
         $departamento->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
