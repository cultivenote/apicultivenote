<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cliente;
use Response;
class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Cliente::all()], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('nombre') || !$request->input('cedula') || !$request->input('apellido') || !$request->input('direccion') || !$request->input('telefono') || !$request->input('correo')|| !$request->input('balance_credito')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevocliente=Cliente::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevocliente]), 201)->header('Location', url('/v1/clientes/'.$nuevocliente->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cliente = Cliente::find($id);

        if (!$cliente) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un cliente con ese id.'])],404);
        }

        return response()->json(['data'=>$cliente],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $cliente=Cliente::find($id);
        if (!$cliente)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un cliente con ese código.'])],404);
        }   
        $nombre=$request->input('nombre');
        $cedula=$request->input('cedula');
        $apelido=$request->input('apellido');
        $direccion=$request->input('direccion');
        $telefono=$request->input('telefono');
        $correo=$request->input('correo');
        $balance=$request->input('balance');
        
        
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($nombre)
            {
                $cliente->nombre= $nombre;
                $paso=true;
            }

             if ($cedula)
            {
                $cliente->cedula= $cedula;
                $paso=true;
            }
             if ($apellido)
            {
                $cliente->apellido= $apellido;
                $paso=true;
            }
             if ($direccion)
            {
                $cliente->direccion= $direccion;
                $paso=true;
            }
             if ($telefono)
            {
                $cliente->telefono= $telefono;
                $paso=true;
            }
             if ($correo)
            {
                $cliente->correo= $correo;
                $paso=true;
            }
             if ($balance)
            {
                $cliente->balance_credito= $balance;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $cliente->save();
                return response()->json(['data'=>$cliente], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de cliente.'])],304);
            }
        }

        if (! $nombre || !$cedula || !$apellido || !$direccion || !$telefono || !$correo|| !$balance_credito)
         {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $cliente->descripcion = $nombre;
        $cliente->cedula = $cedula;
        $cliente->apellido = $apellido;
        $cliente->direccion = $direccion;
        $cliente->telefono = $telefono;
        $cliente->correo = $correo;
        $cliente->balance_credito = $balance;

        // Almacenamos en la base de datos el registro.
        $cliente->save();
        return response()->json(['data'=>$cliente], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
           $cliente = Cliente::find($id);
        if (!$cliente) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un cliente con ese código.'])],404);
            }
        else{
         $cliente->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
