<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Transaccion;
use App\Pedido;
use App\Factura;
use Response;
class TransaccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $facturas = Factura::all();
        $pedidos = Pedido::all();
        $transacciones = Transaccion::with('facturas','pedidos');
        return response()->json(['data'=>$transacciones], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('factura_id') || !$request->input('pedido_id') || !$request->input('consecutivo') || !$request->input('fecha_registro')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevo=Transaccion::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevo]), 201)->header('Location', url('/v1/transaciones/'.$nuevo->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $transacion = Transaccion::find($id);

        if (!$transacion) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un transacion con ese id.'])],404);
        }

        return response()->json(['data'=>$transacion],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $transacion=Transaccion::find($id);
        if (!$transacion)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un cliente con ese código.'])],404);
        }   
        $factura=$request->input('factura_id');
        $pedido=$request->input('pedido_id');
        $consecutivo=$request->input('consecutivo');
        $fecha_registro=$request->input('fecha_registro');
       
        
        
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($factura)
            {
                $transacion->factura_id= $factura;
                $paso=true;
            }

             if ($pedido)
            {
                $transacion->pedido_id= $pedido;
                $paso=true;
            }
             if ($consecutivo)
            {
                $transacion->consecutivo= $consecutivo;
                $paso=true;
            }
             if ($fecha_registro)
            {
                $transacion->fecha_registro= $fecha_registro;
                $paso=true;
            }
            

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $transacion->save();
                return response()->json(['data'=>$transacion], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de transacion.'])],304);
            }
        }

        if (! $factura || !$pedido || !$consecutivo || !$fecha_registro )
         {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $transacion->factura_id = $factura;
        $transacion->pedido_id = $pedido;
        $transacion->consecutivo = $consecutivo;
        $transacion->fecha_registro = $fecha_registro;
        

        // Almacenamos en la base de datos el registro.
        $transacion->save();
        return response()->json(['data'=>$transacion], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $transacion = Transaccion::find($id);
        if (!$transacion) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un transacion con ese código.'])],404);
        }
        else{
         $transacion->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }

    }
}
