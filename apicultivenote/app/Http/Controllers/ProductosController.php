<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Producto;
class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Producto::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('nombre') || !$request->input('descripcion') || !$request->input('precio')|| !$request->input('existencia')|| !$request->input('fecha_vencimiento')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevoproducto=Producto::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevoproducto]), 201)->header('Location', url('/v1/productos/'.$nuevoproducto->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $producto = Producto::find($id);

        if (!$producto) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un producto con ese id.'])],404);
        }

        return response()->json(['data'=>$producto],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $producto=Producto::find($id);
        if (!$producto)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un producto con ese código.'])],404);
        }   

        $nombre=$request->input('nombre');
        $descripcion=$request->input('descripcion');
        $precio=$request->input('precio');
        $existencia=$request->input('existencia');
        $fecha=$request->input('fecha_vencimiento');

        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($nombre)
            {
                $producto->nombre= $nombre;
                $paso=true;
            }
            if ($descripcion)
            {
                $producto->descripcion= $descripcion;
                $paso=true;
            }
            if ($precio)
            {
                $producto->precio= $precio;
                $paso=true;
            }

            if ($existencia)
            {
                $producto->existencia = $existencia;
                $paso=true;
            }
             if ($fecha)
            {
                $producto->fecha_vencimiento = $fecha;
                $paso=true;
            }
            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $producto->save();
                return response()->json(['data'=>$producto], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de producto.'])],304);
            }
        }

        if (!$nombre || !$descripcion || !$precio || !$existencia || !$fecha )
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $producto->nombre = $nombre;
        $producto->descripcion = $descripcion;
        $producto->precio = $precio;
        $producto->existencia = $existencia;
        $producto->fecha_vencimiento = $fecha;
      

        // Almacenamos en la base de datos el registro.
        $producto->save();
        return response()->json(['data'=>$producto], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $producto = Producto::find($id);
        if (!$producto) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un producto con ese código.'])],404);
 }
        else{
         $producto->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
