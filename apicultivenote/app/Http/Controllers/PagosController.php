<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pago;
use Response;
class PagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['data'=>Pago::all()], 200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (! $request->input('empleado_id') || !$request->input('tipo_trabajo_id') || !$request->input('cantidad_laborada')|| !$request->input('pago')|| !$request->input('fecha')) {
            # code...

            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos necesarios para el proceso..'])],422);
        }

        $nuevopago=Pago::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevopago]), 201)->header('Location', url('/v1/pagos/'.$nuevopago->id))->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         $pago = Pago::find($id);

        if (!$pago) {
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encontro un pago con ese id.'])],404);
        }

        return response()->json(['data'=>$pago],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pago=Pago::find($id);
        if (!$pago)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un pago con ese código.'])],404);
        }   

        $empleado=$request->input('empleado_id');
        $tipotrabajo=$request->input('tipo_trabajo_id');
        $cantidad=$request->input('cantidad_laborada');
        $pago=$request->input('pago');
   
        $fecha=$request->input('fecha');
        
        if ($request->method() === 'PATCH')
        {
           
            $paso = false;

            // Actualización parcial de campos.
            if ($empleado)
            {
                $pago->empleado_id= $empleado;
                $paso=true;
            }
            if ($tipotrabajo)
            {
                $pago->tipo_trabajo_id= $tipotrabajo;
                $paso=true;
            }
            if ($cantidad)
            {
                $pago->cantidad_laborada= $cantidad;
                $paso=true;
            }
            if ($pago)
            {
                $pago->pago= $pago;
                $paso=true;
            }
           
            if ($fecha)
            {
                $pago->fecha = $fecha;
                $paso=true;
            }

            if ($paso)
            {
                // Almacenamos en la base de datos el registro.
                $pago->save();
                return response()->json(['data'=>$pago], 200);
            }
            else
            {
        
                // Este código 304 no devuelve ningún body, así que si quisiéramos que se mostrara el mensaje usaríamos un código 200 en su lugar.
                return response()->json(['errors'=>array(['code'=>304,'message'=>'No se ha modificado ningún dato de pago.'])],304);
            }
        }

        if (!$empleado || !$tipotrabajo || !$cantidad|| !$pago|| !$fecha)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 422 Unprocessable Entity – [Entidad improcesable] Utilizada para errores de validación.
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan valores para completar el procesamiento.'])],422);
        }

        $pago->empleado_id = $empleado;
        $pago->tipo_trabajo_id = $tipotrabajo;
        $pago->cantidad_laborada = $cantidad;
        $pago->pago = $pago;
       
        $pago->fecha = $fecha;
        // Almacenamos en la base de datos el registro.
        $pago->save();
        return response()->json(['data'=>$pago], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $pago = Pago::find($id);
        if (!$pago) {
            # code...
           return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentro un pagó con ese código.'])],404);
            }
        else{
         $pago->destroy();
        return response()->json(['code'=>204,'message'=>'Se ha eliminado correctamente.'],204);
        }
    }
}
