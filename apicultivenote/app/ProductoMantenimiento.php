<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoMantenimiento extends Model
{
    //
     protected $table = 'productomantenimientos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'producto_id','mantenimiento_id','cantidad','fecha_aplicado','observaciones',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function producto(){
        return $this->belongsTo('App\Producto');
    }
    public function mantenimiento(){
        return $this->belongsTo('App\Mantenimiento');
    }

}
