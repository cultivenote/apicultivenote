<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    //
     protected $table = 'pagos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'empleado_id','tipo_trabajo_id','cantidad_laborada','pago','fecha',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function empleado(){
        return $this->belongsTo('App\Empleado');
    }
    public function tipotrabajo(){
        return $this->belongsTo('App\TipoTrabajo');
    }
}
