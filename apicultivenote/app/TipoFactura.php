<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoFactura extends Model
{
  

    protected $table = 'tipofacturas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo','descripcion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function facturadetalles(){
        return $this->hasMany('App\FacturaDetalle');
    }
}
