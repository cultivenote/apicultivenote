<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapa extends Model
{
    //
    protected $table = 'mapas';


     protected $fillable = [
        'codigo_mapa','latlng','title','snippet',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

   
}
