<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoSiembrado extends Model
{
    //
     protected $table = 'productosembrados';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'siembra_id','producto_id','cantidad','fecha_registro','observaciones',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function siembra(){
        return $this->belongsTo('App\Siembra');
    }
    public function produto(){
        return $this->belongsTo('App\Producto');
    }
}
