<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductoDetalle extends Model
{
    //
     protected $table = 'productodetalles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'producto_id','categoria_id','departamento_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function departamento(){

        return $this->belongsTo('App\Departamento');
    }
    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }
    public function producto(){
        return $this->belongsTo('App\Producto');
    }


}
