<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siembra extends Model
{
    //
     protected $table = 'siembras';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campo_id','descripcion','consecutivo','fecha_registro',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function campo(){
        return $this->belongsTo('App\Campo');
    }
    public function productosembrados(){
        return $this->hasMany('App\ProductoSembrado');
    }
}
