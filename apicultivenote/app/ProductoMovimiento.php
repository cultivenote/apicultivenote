<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoMovimiento extends Model
{
    //
     protected $table = 'productomovimientos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'producto_id','consecutivo','cantidad_entrante','cantidad_saliendo','descripcion','fecha',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function producto(){
        return $this->belongsTo('App\Producto');
    }
}
