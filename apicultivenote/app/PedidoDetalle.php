<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoDetalle extends Model
{
    //
    

    protected $table = 'pedidodetalles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pedido_id','producto_id','cantidad','descripcion','subtotal',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function producto(){
        return $this->belongsTo('App\Producto');
    }

    public function pedido(){
        return $this->belongTo('App\Pedido');
    }
}
