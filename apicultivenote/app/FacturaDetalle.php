<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaDetalle extends Model
{
    //


    protected $table = 'facturadetalles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'producto_id','factura_id','tipo_factura_id','cantidad','precio_unitario','gravado_ivi','subtotal','descripcion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function producto(){
        return $this->belongsTo('APp\Producto');
    }
    public function factura(){
        return $this->belongsTo('App\Factura');
    }
    public function tipofactura(){
        return $this->belongsTo('App\TipoFactura');
    }

    
}
