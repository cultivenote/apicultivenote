<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    //
     
    protected $table = 'facturas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cliente_id','codigo_factura','consecutivo','fecha_registro','total',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function transacciones(){
        return $this->hasMany('App\Transaccion');
    }
    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }
    public function facturadetalles(){
        return $this->hasMany('App\FacturaDetalle');
    }
}
