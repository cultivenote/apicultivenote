<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mantenimiento extends Model
{
    //
     protected $table = 'mantenimientos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campo_id','descripcion','fecha_registro',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function productomantenimiento(){
        return $this->hasMany('App\ProductoMantenimiento');
    }
    public function campo(){
        return $this->belongsTo('App\Campo');
    }
}
