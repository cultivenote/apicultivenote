<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table = 'clientes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','cedula','apellido','direccion','telefono','correo','balance_credito',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function facturas(){
        return $this->hasMany('App\Factura');
    }
    public function pedidos(){
        return $this->hasMany('App\Pedido');
    }
}
