<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campo extends Model
{
    //

    protected $table = 'campos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','duenno','title','latitud','longitud','descripcion','direccion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function siembras(){
        return $this->hasMany('App\Siembra');
    }

    public function mantenimientos(){
        return $this->hasMany('App\Mantenimiento');
    }
}
