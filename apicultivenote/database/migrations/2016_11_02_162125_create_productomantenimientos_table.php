<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductomantenimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productomantenimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('producto_id')->unsigned();
            $table->integer('mantenimiento_id')->unsigned();
            $table->double('cantidad');
            $table->dateTime('fecha_aplicado');
            $table->string('observaciones');
            $table->timestamps();
            
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');

            $table->foreign('mantenimiento_id')->references('id')->on('mantenimientos')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productomantenimientos');
    }
}
