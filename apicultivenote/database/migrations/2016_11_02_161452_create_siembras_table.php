<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiembrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siembras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campo_id')->unsigned();
            $table->string('descripcion');
            $table->integer('consecutivo');
            $table->dateTime('fecha_registro');
            $table->timestamps();

            $table->foreign('campo_id')->references('id')->on('campos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siembras');
    }
}
