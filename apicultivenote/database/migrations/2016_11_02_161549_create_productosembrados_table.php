<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosembradosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productosembrados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siembra_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->double('cantidad');
            $table->dateTime('fecha_registro');
            $table->string('observaciones');
            $table->timestamps();

            $table->foreign('siembra_id')->references('id')->on('siembras')->onDelete('cascade');
            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productosembrados');
    }
}
