<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturadetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturadetalles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('producto_id')->unsigned();
            $table->integer('factura_id')->unsigned();
            $table->integer('tipo_factura_id')->unsigned();
            $table->double('cantidad');
            $table->double('precio_unitario');
             $table->double('gravado_ivi');
              $table->double('subtotal');
               $table->double('total');
               $table->string('descripcion',100);
            $table->timestamps();


            $table->foreign('producto_id')->references('id')->on('productos')->onDelete('cascade');
            $table->foreign('factura_id')->references('id')->on('facturas')->onDelete('cascade');
            $table->foreign('tipo_factura_id')->references('id')->on('tipofacturas')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturadetalles');
    }
}
